package home;
/*
 * 作业一、
 编写一个Book类，该类至少有name和price两个属性。
 该类要实现Comparable接口，在接口的compareTo（）方法中
 规定两个Book类实例的大小关系为二者的price属性的大小关系。
 在主方法main中，选择LinkedList集合类型存放Book类的若干个对象，
 然后创建一个新的Book类的对象，并检查该对象与集合中的哪些对象相等。

 */
public class Book implements Comparable{
	
	private String name;
	
	private double price;
	
	public Book(String name, int price) {
		this.name=name;
		this.price=price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int compareTo(Object obj)
	{
		Book book=(Book)obj;
		if(this.price==book.price)
		{
			return 0;
		}else if(this.price>book.price)
		{
			return 1;
		}else
		{
			return -1;
		}
		
	}
	

}
