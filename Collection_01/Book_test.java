package home;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;
public class Book_test {

	public static void main(String[] args) {
		LinkedList bookList=new LinkedList();
		bookList.add(new Book("《春风》",58));
		bookList.add(new Book("《夏虎》",34));
		bookList.add(new Book("《秋菊》",88));
		bookList.add(new Book("《冬谷》",12));
		bookList.add(new Book("《华商报》",35));
		
		System.out.println("------排序前------");
		System.out.println("书名"+"\t"+"价格");
		Iterator it=bookList.iterator();
		while(it.hasNext())
		{
			Book book=(Book)it.next();
			System.out.println(book.getName()+"\t"+book.getPrice());
		}
		Collections.sort(bookList);
		System.out.println("------排序后------");
		System.out.println("书名"+"\t"+"价格");
		Iterator it2=bookList.iterator();
		while(it2.hasNext())
		{
			Book book=(Book)it2.next();
			System.out.println(book.getName()+"\t"+book.getPrice());
		}

	}

}
