package home;
/*
 * 码云作业二、
写一个MyStack 类，表示“堆栈”这种数据结构。堆栈的特点：先进后出。
堆栈在表示上，就如同一个单向开口的盒子，每当有新数据进入时，都是进入栈顶。其基
本操作为push 和pop。push 表示把一个元素加入栈顶，pop 表示把栈顶元素弹出。使用LinkedList集合类实现
栈的基本操作：
1） push(Object o)：表示把元素放入栈
2） Object pop()：返回栈顶元素，并把该元素从栈中删除。如果栈为空，则返回 null 值
3） Object peek()：返回栈顶元素，但不把该元素删除。如果栈为空，则返回null值。
4） boolean isEmpty()：判断该栈是否为空
5） int size()：返回该栈中元素的数量
 */

import java.util.LinkedList;
public class MyStack {

	private LinkedList list=new LinkedList();
	
	
	public LinkedList getList() {
		return list;
	}


	public void setList(LinkedList list) {
		this.list = list;
	}


	public void push(Object o)
	{
		this.list.add(o);
	}
	
	public Object pop()
	{
		if(this.list.isEmpty())
		{
			return null;
		}
		return this.list.removeFirst();
	}
	
	public Object peek()
	{
		if(this.list.isEmpty())
		{
			return null;
		}
		return this.list.getFirst();
	}
	public boolean isEmpty()
	{
		if(this.list.isEmpty())
		{
			return true;
		}else
		{
			return false;
		}
	}
	public int size()
	{
		return this.list.size();
	}
	
}
