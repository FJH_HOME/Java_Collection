package home;
/*
 * 作业三、
已知某学校的教学课程内容安排如下：
老师姓名   课程名称
Tom             Java
John            Oracle
Alice            Jsp
Lucy            Jsp
Jim              Linux
Kevin          Jsp

完成下列要求：
1） 使用一个HashMap，以老师的名字作为键Key，以老师教授的课程名作为值Value，表示上述
课程安排。
2） 增加了一位新老师Allen 教SSM
3） Lucy 改为教Java
4） 遍历HashMap，输出所有的老师及老师教授的课程
5） 使用HashMap，输出所有教JSP 的老师姓名及课程名。
 */
import java.util.HashMap;
public class Teachers {

	public static void main(String[] args) {
		HashMap list=new HashMap();
		list.put("Tom", "Java");
		list.put("John", "Oracle");
		list.put("Alice", "Jsp");
		list.put("Lucy", "Jsp");
		list.put("Jim", "Linux");
		list.put("Kevin", "Jsp");
		System.out.println("-------------------");
		System.out.println("姓名"+"\t"+"教授课程");
		for(Object obj:list.keySet())
		{
			System.out.println(obj+"\t"+list.get(obj));
		}
		System.out.println("-------------------");
		list.put("Allen","SSM");
		System.out.println("-------------------");
		System.out.println("姓名"+"\t"+"教授课程");
		for(Object obj:list.keySet())
		{
			System.out.println(obj+"\t"+list.get(obj));
		}
		System.out.println("-------------------");
        list.put("Lucy", "Java");
        System.out.println("-------------------");
		System.out.println("姓名"+"\t"+"教授课程");
		for(Object obj:list.keySet())
		{
			System.out.println(obj+"\t"+list.get(obj));
		}
		System.out.println("-------------------");
		System.out.println("姓名"+"\t"+"教授课程");
		for(Object obj:list.keySet())
		{
			if(list.get(obj).equals("Jsp"))
			{
				System.out.println(obj+"\t"+list.get(obj));
			}
		}
		System.out.println("-------------------");
	}

}
