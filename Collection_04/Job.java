package home;

import java.util.Date;


/*
 * 作业四、
实现招聘信息的存储
1）招聘信息类  Job  其中包含如下属性：
职位编号(int jobId)、String jobName(职位名称)，industry （行业）、
招聘人数（int number）、发表时间(Date pubDate)。
2)、使用HashMap存储,创建5条招聘信息对象，
显示招聘信息的方法showJobList()，
根据职位名模糊查询职位信息方法searchJobByName(String jobName)，
按pubDate发表时间降序排序的方法，
提示:自定义一个类(PubDateComparator)实现
java.util.Comparator比较器接口和Collections.sort()方法。
 */
public class Job implements Comparable{

	private String jobName;
	private String industry;
	private int number;
	private Date pubDate;
	public Job()
    {
    	
    }
	
	public Job( String jobName, String industry, int number, Date pubDate) {
		super();
		
		this.jobName = jobName;
		this.industry = industry;
		this.number = number;
		this.pubDate = pubDate;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Date getPubDate() {
		return pubDate;
	}
	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}

	public int compareTo(Object o) {
		Job job=(Job)o;
		if(this.pubDate.equals(job.pubDate))
		{
			return 0;
		}else if(this.pubDate.after(job.pubDate))
		{
			return 1;
		}else
		{
			return -1;
		}
	}

	
}
