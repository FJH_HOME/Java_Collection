package home;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Job_Test {
	
	public static HashMap<Integer,Job> list=new HashMap<Integer,Job>();
	
	public static void showJobList()
	{
		System.out.println("职位编号"+"\t"+"职位名称"+"\t"+"行业"+"\t"+"招聘人数"+"\t"+"发布时间");
		for(Object obj:list.keySet())
		{
		Job job=(Job)list.get(obj);
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(obj+"\t"+job.getJobName()+"\t"+job.getIndustry()+"\t"+job.getNumber()+"\t"+
	format.format(job.getPubDate()));
		}
	}
	
	public static void searchJobByName(String jobName)
	{
		System.out.println("模糊查询结果：");
		System.out.println("职位编号"+"\t"+"职位名称"+"\t"+"行业"+"\t"+"招聘人数"+"\t"+"发布时间");
		for(Object obj:list.keySet())
		{
		Job job=(Job)list.get(obj);
		if(job.getJobName().indexOf(jobName)!=-1)
		{
			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			System.out.println(obj+"\t"+job.getJobName()+"\t"+job.getIndustry()+"\t"+job.getNumber()+"\t"+
		format.format(job.getPubDate()));
		}
		}
	}
	
	
	public static void data()
	{
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Date date=new Date();

		list.put(1, new Job("汽车修理","服务",2,date));
		data();
		date=new Date();
		list.put(2, new Job("电脑修理","服务",3,date));
		data();
		date=new Date();
		list.put(3, new Job("家具清洁","服务",5,date));
		data();
		date=new Date();
		list.put(4, new Job("跑腿服务","服务",5,date));
		data();
		date=new Date();
		list.put(5, new Job("游戏代练","服务",10,date));
		
		showJobList();
		searchJobByName("腿");

	}

}
